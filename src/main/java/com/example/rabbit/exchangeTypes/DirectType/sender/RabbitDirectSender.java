package com.example.rabbit.exchangeTypes.DirectType.sender;

import com.example.rabbit.model.QueueItem;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RabbitDirectSender {
    @Autowired
    RabbitTemplate rabbitTemplate;

    public void publish(QueueItem queueItem){
        rabbitTemplate.convertAndSend("first-exchange","first-routing-key",queueItem.toJson());
        //first exchange,first-routing-key ile bağlanmış olan kuyruklara mesaj iletecek
    }

}
