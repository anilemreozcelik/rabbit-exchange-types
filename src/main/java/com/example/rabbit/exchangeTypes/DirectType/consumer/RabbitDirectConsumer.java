package com.example.rabbit.exchangeTypes.DirectType.consumer;

import com.example.rabbit.model.QueueItem;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RabbitDirectConsumer {

    @RabbitListener(queues = {"first-queue","second-queue","third-queue"})
    public void consume(String message, @Header(AmqpHeaders.CHANNEL) Channel channel,@Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag){
        try {
            System.out.println(message);
            QueueItem item=new QueueItem(message);
            channel.basicAck(deliveryTag,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
