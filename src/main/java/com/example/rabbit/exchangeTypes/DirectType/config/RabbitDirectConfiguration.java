package com.example.rabbit.exchangeTypes.DirectType.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitDirectConfiguration {

    //Burada 1 adet exchange tanımlayıp bu exchange'i birden fazla queue'ya bind edeceğiz.
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("first-exchange");
    }

    @Bean
    public Queue thirdQueue(){
        return new Queue("third-queue");
    }

    @Bean
    public Queue secondQueue(){
        return new Queue("second-queue");
    }

    @Bean
    public Queue firstQueue(){
        return new Queue("first-queue");
    }


    @Bean
    public Binding binding1(){
        return BindingBuilder.bind(firstQueue()).to(directExchange()).with("first-routing-key");
    }

    @Bean
    public Binding binding2(){
        return BindingBuilder.bind(secondQueue()).to(directExchange()).with("first-routing-key");
    }

    @Bean
    public Binding binding3(){
        return BindingBuilder.bind(thirdQueue()).to(directExchange()).with("third-routing-key");
    }
}
