package com.example.rabbit.exchangeTypes.DefaultType.sender;

import com.example.rabbit.model.QueueItem;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RabbitDefaultSender {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Value("${queueName}")
    private String queueName;

    public void publish(QueueItem queueItem){
        rabbitTemplate.convertAndSend(queueName,queueItem.toJson());  //aslında ilk parametre routingkey
    }
}
