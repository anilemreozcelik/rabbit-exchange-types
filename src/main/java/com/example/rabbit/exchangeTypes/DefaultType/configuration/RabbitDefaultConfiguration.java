package com.example.rabbit.exchangeTypes.DefaultType.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitDefaultConfiguration {

    @Bean
    public Queue queue(){
        return new Queue("my-queue");
    }

}
