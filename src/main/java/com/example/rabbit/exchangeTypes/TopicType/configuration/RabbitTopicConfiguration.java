package com.example.rabbit.exchangeTypes.TopicType.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitTopicConfiguration {

    @Bean
    public Queue healthQueue(){
        return new Queue("health-queue");
    }

    @Bean
    public Queue sportsQueue(){
        return new Queue("sports-queue");
    }

    @Bean
    public Queue educationQueue(){
        return new Queue("education-queue");
    }

    @Bean
    public TopicExchange exchange(){
        return new TopicExchange("topic-exchange");
    }

    @Bean
    public Binding binding4(){
        return BindingBuilder.bind(healthQueue()).to(exchange()).with("health.*");
    }

    @Bean
    public Binding binding5(){
        return BindingBuilder.bind(sportsQueue()).to(exchange()).with("#.sports.*");
    }

    @Bean
    public Binding binding6(){
        return BindingBuilder.bind(educationQueue()).to(exchange()).with("#.education");
    }
}
