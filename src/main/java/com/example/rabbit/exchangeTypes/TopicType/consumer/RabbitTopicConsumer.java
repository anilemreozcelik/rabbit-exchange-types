package com.example.rabbit.exchangeTypes.TopicType.consumer;

import com.example.rabbit.model.QueueItem;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RabbitTopicConsumer {

    @RabbitListener(queues = {"health-queue"})
    public void healthConsumer(String message, @Header(AmqpHeaders.CHANNEL) Channel channel,@Header(AmqpHeaders.DELIVERY_TAG)long deliveryTag){
        try {
            System.out.println("health-queue:"+message);
            QueueItem item=new QueueItem(message);
            channel.basicAck(deliveryTag,false);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @RabbitListener(queues = {"sports-queue"})
    public void sportsConsumer(String message, @Header(AmqpHeaders.CHANNEL) Channel channel,@Header(AmqpHeaders.DELIVERY_TAG)long deliveryTag){
        try {
            System.out.println("sports-queue:"+message);
            QueueItem item=new QueueItem(message);
            channel.basicAck(deliveryTag,false);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @RabbitListener(queues = {"education-queue"})
    public void educationConsumer(String message, @Header(AmqpHeaders.CHANNEL) Channel channel,@Header(AmqpHeaders.DELIVERY_TAG)long deliveryTag){
        try {
            System.out.println("education-queue:"+message);
            QueueItem item=new QueueItem(message);
            channel.basicAck(deliveryTag,false);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
