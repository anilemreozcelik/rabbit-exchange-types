package com.example.rabbit.exchangeTypes.TopicType.producer;

import com.example.rabbit.model.QueueItem;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class RabbitTopicProducer {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    TopicExchange topicExchange;

    AtomicInteger index = new AtomicInteger(0);

    AtomicInteger count = new AtomicInteger(0);

    private final String[] keys = {"health.sports.rabbit", "test.education", "test.sports.rabbit",
            "health.education", "lazy.sports.education"};

    @Scheduled(fixedDelay = 1000, initialDelay = 500)
    public void publish(QueueItem queueItem){
        if (this.index.incrementAndGet() == keys.length) {
            this.index.set(0);
        }
        String key = keys[this.index.get()];
        rabbitTemplate.convertAndSend(topicExchange.getName(),key,queueItem.toJson());
    }
}
