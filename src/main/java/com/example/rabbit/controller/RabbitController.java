package com.example.rabbit.controller;

import com.example.rabbit.exchangeTypes.DefaultType.sender.RabbitDefaultSender;
import com.example.rabbit.exchangeTypes.DirectType.sender.RabbitDirectSender;
import com.example.rabbit.exchangeTypes.TopicType.producer.RabbitTopicProducer;
import com.example.rabbit.model.QueueItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/rabbit")
public class RabbitController {
    @Autowired
    RabbitDefaultSender rabbitDefaultSender;
    @Autowired
    RabbitDirectSender rabbitDirectSender;
    @Autowired
    RabbitTopicProducer rabbitTopicProducer;

    @GetMapping("default")
    public void defaultExchange(){
        QueueItem item=new QueueItem("1","Anil","Ozcelik",30);
        rabbitDefaultSender.publish(item);
    }

    @GetMapping("direct")
    public void directExchange(){
        QueueItem item=new QueueItem("1","Anil","Ozcelik",30);
        rabbitDirectSender.publish(item);
    }

    @GetMapping("topic")
    public void topicExchange(){
        QueueItem item=new QueueItem("1","Anil","Ozcelik",30);
        rabbitTopicProducer.publish(item);
    }
}
