package com.example.rabbit.model;

import org.json.JSONException;
import org.json.JSONObject;

public class QueueItem {

    private String id;
    private String name;
    private String lastName;
    private int age;

    public QueueItem(String id, String name, String lastName, int age) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public QueueItem(String message) {

        JSONObject object=new JSONObject(message);
        this.id=object.getString("id");
        this.name=object.getString("name");
        this.lastName=object.getString("lastName");
        this.age=object.getInt("age");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toJson(){

        try {
            JSONObject object=new JSONObject();
            object.put("id",id);
            object.put("name",name);
            object.put("lastName",lastName);
            object.put("age",age);
            return object.toString();
        }catch (JSONException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String toString() {
        return "QueueItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
